import express, { json, urlencoded } from 'express';
import * as dgraph from "dgraph-js-http";
import { alpha, CATEGORY_LIST } from './config';
var cors = require('cors')


// // const Schema = require('./dgraph/Schema');
// var express = require('express');
var { graphqlHTTP } = require('express-graphql');
var { buildSchema } = require('graphql');

// Construct a schema, using GraphQL schema language
var mySchema = buildSchema(`
  type Query {
    Account: Account
    accounts: [Account]
    categories: [Category],
    suburbs: [Suburb],
    brands: [Brand],
    getFilteredAccounts(brand: String, category: String, postcode: String): [Account]
  }

  type Suburb {
    id: ID!
    name: String!
    postcode: String!
  }
  type Category {
    id: ID!
    name: String!
  }
  type Brand {
    id: ID!
    name: String!
  }

  type Todo {
      title:String!
      body:String!
      complete: Boolean!
  }
  type Account {
      name:String!
      remote_id:String!
      address: Address
      categories: [Category]
      brands: [Brand]
      subburbs: [Suburb]
  }

  type Address {
    suburb: String
    street: String
    postCode: String
  }
  type BlogPost {
    title: String
    content: String
  }
`);
// The root provides a resolver function for each API endpoint
var root = {

  accounts: async (root, args, context) => {
      const query = `{
      accounts(func: has(Account.remote_id)) @cascade {
          remote_id: Account.remote_id
          name: Account.legal_name
      }
    }`;
    try {
      const clientStub = new dgraph.DgraphClientStub(alpha);
      const dgraphClient = new dgraph.DgraphClient(clientStub);

      const response = await dgraphClient.newTxn().query(query);
      console.log("accounts data---", response.data)
      
    const {data: {accounts}} = response

      return accounts
    } catch (e) {
      console.log('exception ====> ', e);
    }
  },


  categories: async (root, args, context) => {
      const query = `{
      categories(func: has(Category.name)) {
        id : uid
        name: Category.name
      }
    }`;
    try {
      const clientStub = new dgraph.DgraphClientStub(alpha);
      const dgraphClient = new dgraph.DgraphClient(clientStub);
      const response = await dgraphClient.newTxn().query(query);
    const {data: {categories}} = response

      return categories
    } catch (e) {
      console.log('exception ====> ', e);
    }
  },

  postcodes: async (root, args, context) => {
      const query = `{
  postcodes(func: has(Suburb.name)) {
    name: Suburb.name
    Suburb.postcode
  }
}`;
    try {
      const clientStub = new dgraph.DgraphClientStub(alpha);
      const dgraphClient = new dgraph.DgraphClient(clientStub);

      const response = await dgraphClient.newTxn().query(query);
      
    const {data: { postcodes }} = response
      return postcodes
    } catch (e) {
      console.log('exception ====> ', e);
    }
  },

  brands: async (root, args, context) => {
      const query = `{
  brands(func: has(Brand.name)) {
    id : uid
    name: Brand.name
  }
}`;

    console.log('START ======>');
    try {
      const clientStub = new dgraph.DgraphClientStub(alpha);
      const dgraphClient = new dgraph.DgraphClient(clientStub);

      const response = await dgraphClient.newTxn().query(query);
      
    const {data: { brands }} = response
    console.log("brand ----<>>", response.data)

      return brands
    } catch (e) {
      console.log('exception ====> ', e);
    }
  },

  suburbs: async (root, args, context) => {
      const query = `{
    suburbs(func: has(Suburb.name)) {
    name: Suburb.name
    postcode: Suburb.postcode
  }
}`;
    try {
      const clientStub = new dgraph.DgraphClientStub(alpha);
      const dgraphClient = new dgraph.DgraphClient(clientStub);
      const response = await dgraphClient.newTxn().query(query);
      return response.data.suburbs
    } catch (e) {
      console.log('exception ====> ', e);
    }
  },
  getFilteredAccounts: async (root, args, context) => {
    const variables = args.body.variables;
    const { category = "roofing", brand="Brand", postcode="2194"} = variables.payload;
    console.log("variables updated", variables.payload);

    const query = `query accounts($category: string, $brand: string, $postcode: string) {
    accounts(func: has(Account.remote_id))@cascade {
      remote_id: Account.remote_id
      name: Account.legal_name
      Account.categories @filter(eq(Category.name, $category)) @cascade {
        Category.name
      }
     Account.brands @filter(eq(Brand.name, $brand)) @cascade  {
      Brand.name
      }
      Account.serviceArea {
      ServiceArea.name
      ServiceArea.suburbs @filter (eq(Suburb.postcode, $postcode)) {
        Suburb.name
        Suburb.postcode
            }
        }
    } #accounts search ends
}
    `;

    const queryVariables = { $category : category, $brand : brand, $postcode: postcode}
    console.log("queryVariables", queryVariables)
    try {
      const clientStub = new dgraph.DgraphClientStub(alpha);
      const dgraphClient = new dgraph.DgraphClient(clientStub);
      const response = await dgraphClient.newTxn().queryWithVars(query, queryVariables)
      console.log("accounts ----<>>", response.data.accounts)
      return response.data.accounts
    } catch (e) {
      console.log('exception ====> ', e);
    }
  },
};

const app = express();
const PORT = 9595;
app.use(express.json());
app.use(cors())
app.use(express.urlencoded({ extended: true }));

//enable pre-flight across-the-board
app.use(
  '/graphql',
  graphqlHTTP({
    schema: mySchema,
    rootValue: root,
    graphiql: true,
  })
);
app.get('/schema/add', async (req, res) => {
  const ENDPOINT = 'http://localhost:8080/admin/schema';
  Axios.post(ENDPOINT, dgraphSchema).then(response => console.log("response", response)).catch(err => console.log("error", err));
  res.send({ response: "Done adding Schema" }).status(200);
})

app.listen(PORT, function () {
  console.log(`Listening on ${PORT}`);
});
